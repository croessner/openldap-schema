# OpenLDAP schema

This is a set of OpenLDAP schema files

## Mail

The mail schema was created more than 10 years ago and is used in production over all
the years. It still gets updates from time to time.

The rnsmailsystem.schema schema was designed for Postfix, Dovecot and OpenDKIM
purposes. It contains the following classes:

* rnsMSPostfixAccount
* rnsMSDovecotAccount
* rnsMSPostfixGroup
* rnsMSDKIM
* rnsMSGeneralPurpose

### Example LDIF

```
dn: uid=test1,ou=people,ou=it,dc=roessner-net,dc=de
objectClass: top
objectClass: inetOrgPerson
objectClass: extensibleObject
objectClass: rnsMSDovecotAccount
objectClass: rnsMSPostfixAccount
cn: Whatever
rnsMSDovecotUser: de01@exampleserver.de
sn: Testaccount 1
mail: test1@exampleserver.de
rnsMSAbuseId: Ani5ahDeePhuopohkushe8equ4iequoh.customer
rnsMSDeliverToAddress: de01@exampleserver.de
rnsMSEnableDovecot: FALSE
rnsMSEnablePostfix: FALSE
rnsMSMailPath: sdbox:~/sdbox
rnsMSPostfixRateLimit: 50
rnsMSPwdResetMail: someremote@example.test
rnsMSQuota: 5242880
rnsMSRecipientAddress: de01@exampleserver.de
rnsMSRecipientAddress: test1@exampleserver.de
uid: test1
uniqueIdentifier: test1
userPassword: what_ever_password
```

#### rnsMSDovecotUser: de01@exampleserver.de

This is the Dovecot user. It is typically used for the iterator in Dovecot. Here
is the example Dovecot definition:

```
iterate_attrs = \
        =user=%{ldap:rnsMSDovecotUser}
iterate_filter = (&(objectClass=rnsMSDovecotAccount)(!(rnsMSDovecotMaster=TRUE))(rnsMSEnableDovecot=TRUE))
```

In my case, master users are also stored in OpenLDAP ;-) I do not want to make them real user accounts, so the
flag rnsMSDovecotMaster marks an account as a master account.

#### rnsMSAbuseId: Ani5ahDeePhuopohkushe8equ4iequoh.customer

This is a random string that is used by Postfix to add a unique header for tracing purposes:

```
query_filter = (&(objectClass=rnsMSPostfixAccount)(rnsMSRecipientAddress=%s))
result_attribute = rnsMSAbuseId
result_format = PREPEND X-Abuse-Id: %s
```

Sample definition in Postfix main.cf:

```main.cf
smtpd_sender_restrictions =
    check_sender_access ${ldap}/prepend_id.cf,
    ...
```

#### rnsMSDeliverToAddress: de01@exampleserver.de, rnsMSRecipientAddress:...

This attribute is for Postfix to find out the final destination. It can be used
in virtual_alias to root different mails (rnsMSRecipientAddress) to that address.

As a design decision, I have one internal domain. Each user does have a alphanumeric
localpart. All other email address (localpart and domains) are routed to this internal
domain. The goal is that you can add and remove domains as you like and have no dependency
to a main domain.

Example relay_recipient_maps.cf:

```
query_filter = (&(objectClass=rnsMSPostfixAccount)(|(rnsMSRecipientAddress=%s)))
result_attribute = rnsMSRecipientAddress
```

relay_recipient_maps:

```main.cf
relay_recipient_maps =
    ${ldap}/relay_recipient_maps.cf
```

virtual_alias_maps.cf:

```
query_filter = (&(objectClass=rnsMSPostfixAccount)(|(rnsMSRecipientAddress=%s)))
result_attribute = rnsMSDeliverToAddress
```

virtual_alias:

```main.cf
virtual_alias_maps =
    ${ldap}/virtual_alias_maps.cf
```

#### rnsMSPwdResetMail: someremote@example.test

As I have a self service portal for customers, they can reset there password by sending a link to
an alternative address.

#### rnsMSPostfixRateLimit

This is the maximum amount of mails a user is allowed to sent per day. It is used by the
ratelimit policy service found here on this gitlab server.

#### rnsMSQuota

This is used for quota in Dovecot. See below for an usage example.

#### rnsMSMailPath

This is used by Dovecot as shown here:

```
user_attrs = \
        =quota_rule=*:storage=%{ldap:rnsMSQuota}, \
        =home=%{ldap:rnsMSMailboxHome}, \
        =mail_location=%{ldap:rnsMSMailPath}, \
        =acl_groups=%{ldap:rnsMSACLGroups}
```

#### rnsMSEnableDovecot and rnsMSEnablePostfix

Thes flags can turn off or on Postfix and Dovecot support. I use these in my
other gitlab project authserv, which you can also find here (still not documented,
because it is under heavy development and not yet announced).

The authserv is a authentication server that can be used with Nginx. Here are the filters
that I use in production:

```
# For Dovecot:
(&(objectClass=rnsMSDovecotAccount)(rnsMSEnableDovecot=TRUE)(|(uniqueIdentifier=%s)(rnsMSRecipientAddress=%s)))

# For Postfix:
(&(objectClass=rnsMSPostfixAccount)(rnsMSEnablePostfix=TRUE)(|(uniqueIdentifier=%s)(rnsMSRecipientAddress=%s)))
```

### A Postfix Group example and its usecase:

dn: ou=mailDomains,ou=it,dc=roessner-net,dc=de
objectClass: organizationalUnit
objectClass: rnsMSPostfixGroup
objectClass: top
ou: mailDomains
rnsMSVirtDomain: domain1.tld
rnsMSVirtDomain: domain2.tld
rnsMSVirtDomain: domain3.tld
rnsMSVirtDomain: domain4.tld
rnsMSVirtDomain: ...
rnsMSRejectMsg: REJECT Do not send mail with our domain

#### Short integration example in Postfix

Example relay_domains.cf:

```
query_filter = (&(objectClass=rnsMSPostfixGroup)(rnsMSVirtDomain=%s))
result_attribute = rnsMSRejectMsg
```

relay_domains:

```main.cf
relay_domains =
    ${ldap}/relay_domains.cf
```

You could use the rnsMSRejectMsg attribute to block remote senders that have
one of your domains as there sender domain. I currently make not use of this, but
have still enabled this attrbute.

## Final words

This documentation is still incomplete. I will enhance it over time. If you are
interested in some classes or attributes, feel free to contact me and I will help you.

If you like to improve it, you may send me your ideas...

